# Changelog

## 20.12.0
- First major release
- Bugfixes
- New webview plugin for NC login
- Add french translation

## 0.9.12
- Dark Theme & Amoled Mode
- Bugfixes

## 0.9.11
- UI/UX improvements
- Bugfixes

## 0.9.10
- Bugfixes
- Local Pin Auth
- Add folder tree view screen

## 0.9.9
- Display Favicons
- UI improvements

## 0.9.8
- Nextcloud Theming (provided color scheme by the server)
- Boxes of the list items removed
- Sort passwords and folder by name (case ignored)

## 0.9.7
- Android Autofill support
- Redesign lock screen (local auth screen)
- Loading first cached data, then fetch the data again from the server
- Redesign of Nextcloud login screen
- Bugfixes

## 0.9.5
- New: Move password to different folder
- New: Delete folder

## 0.9.4
- Add yes/no dialog for logout
- Add settings for the password strength
- Add translations: german

## 0.9.3
- Add settings screen:
    - Set start view: All Passwords, Folders or Favorites
    - Enable/Disable biometric auth
- Small bugfixes

## 0.9.2
- Show/Edit passwords (+ delete passwords -> move to trash)
- Show folders (+ create new Folders & rename existing folders)
- Show favorites (toggle favorite status)
